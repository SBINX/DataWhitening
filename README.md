# Data Whitening

## Overview
In this project, we will see how to automatise the creation of a universe of points around an observation to optimaly detect the what perturbation allows for a change in class - therefore allowing for (a) the extraction of the features important for the black-box classifier; but also (b) of information of more practical use in Decision Making in a variety of fields (e.g. we can imagine helping a doctor to understand why a hypothetical deep-learning black-box medical algorithm proposes a solution, therefore having enough insight to follow it or discard it according to its own understanding).


## Getting Started

In this project, we will see how to introduce perturbation within data in order to gain knowledge about a black-box classifier. 

The following instructions will show you how to use/customize this library.

### Prerequisites

What things you need to install the software and how to install them

```
sklearn
Numpy (1.11 or later)
itertool
```

Then you can run 
```
pip install DataWhitening
```

Then simply import the needed class in python
```
from DataWhitening.datawhitening import Collection_points
```


## Running the tests

You will find an [Ipython Notebook](https://gitlab.com/SBINX/DataWhitening/blob/master/DataWhitening.py) that will explain how to use this library. 

It will:
* Download a breast cancer dataset
* Train a model
* Use DataWhitening library over an example observation 


## Authors

* **Seddik BELKOURA** 



## Future Work

This work is still in progress. The author is currently working on:
* improving the robustness to data format
* provide the library through pip 



## License

Please cite this work: 

Fostering interpretability of data mining models through data perturbation. Belkoura, S; Zanin, M, LaTorre, A (2018). Inpress 